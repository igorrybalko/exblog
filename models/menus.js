const pool = require('../ext/db');

function getMenuItems(menuId) {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {

            let query = 'SELECT * FROM `menuitems` WHERE `menuid` = ' + menuId + ' AND `published` = 1';

            connection.connect();
            connection.query( query, (error, results, fields) => {
                connection.release();
                if (error) throw error;

                resolve(results);
                reject('error in models/menus.js');

            });
        });
    });

}

exports.getMenuItems = getMenuItems;