const pool = require('../ext/db'),
      helper = require('../ext/helper');

function _getCatId(data) {
    return new Promise((resolve, reject) => {

        pool.getConnection((err, connection) => {

            let query = "SELECT `id` FROM `categories` WHERE `alias` = '" + data.rootPath + "'";
            connection.connect();
            connection.query(query, (error, result, fields) => {
                connection.release();
                if (error) throw error;
                data.catId = result[0]['id'];

                resolve(data);
                reject('error function _getCatId');

            });
        });
    });
}

function _countItems(data) {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {

            let query = 'SELECT COUNT(`id`) FROM `items` WHERE `category` = ' + data.catId;
            connection.connect();
            connection.query(query, (error, countObj, fields) => {
                connection.release();
                if (error) throw error;
                data.countItems = countObj[0]["COUNT(`id`)"];

                resolve(data);
                reject('error function _countItems');

            });
        });
    });
}

function _getListItems(data){
    return new Promise((resolve, reject) => {
        let itemsOnPage = 5,
            curPage = 1;
        let countPages = Math.ceil(data.countItems/itemsOnPage);


        if(data.numPage > countPages){
            return data.res.redirect('/' + data.rootPath);
        }

        if(data.numPage > 0 && numPage <= countPages){
            curPage = data.numPage;
        }

        let start = (curPage - 1) * itemsOnPage;
        let pagination = {
            countPages,
            rootPath: data.rootPath,
            curPage
        };

        pool.getConnection((err, connection) => {

            let query = "SELECT * FROM `items` WHERE `category` = " + data.catId + " LIMIT " + start + ", " + itemsOnPage;

            connection.connect();
            connection.query( query, (error, results, fields) => {
                connection.release();
                if (error) throw error;

                let datares = {
                    list: results,
                    pagination,
                    catId: data.catId
                };

                resolve(datares);
                reject('error function _getListItems');

            });
        });
    });
}

function _getCategory(props){

    return new Promise((resolve, reject) => {

        pool.getConnection((err, connection) => {

            let query = 'SELECT `description`, `title`, `alias` FROM `categories` WHERE `id` = ' + props.catId;
            connection.connect();
            connection.query(query, (error, results, fields) => {
                connection.release();
                if (error) throw error;
                let data = results[0];
                data.items = props.list;
                data.pagination = props.pagination;

                resolve(data);
                reject('error function _getCategory');

            });
        });
    });

}

function getItem(alias){

    return new Promise((resolve, reject) => {

        pool.getConnection((err, connection) => {
    
            let query = 'SELECT * FROM `items` WHERE `alias` = "' + alias + '"';
            connection.connect();
            connection.query( query, (error, results, fields) => {
                connection.release();
                if (error) throw error;
    
                let data = results[0];
                data.createdate = helper.getDate(data.createdate);
                
                resolve(data);
                reject('error function getItem');
    
            });
        });
    });
}

function getListItems(data){
    return new Promise((resolve, reject) => {

        _getCatId(data)
            .then(response =>{
               return _countItems(response);
            })
            .then(response => {
                return _getListItems(response);
            })
            .then(response => {
               return _getCategory(response);
            })
            .then(response => {
                resolve(response);
                reject('error function getListItems');
            })
            .catch(error => console.log(error));

    });
}

exports.getListItems = getListItems;
exports.getItem = getItem;