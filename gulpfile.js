//base part
var gulp = require('gulp'),
    rename  = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps');

//css part
var sass = require('gulp-sass'),
    cleanCSS = require('gulp-clean-css'),
    autoprefixer = require('gulp-autoprefixer');

//js part
var include = require('gulp-include'),
    uglify  = require('gulp-uglify'),
    babel = require('gulp-babel');

function swallowError(error){
    console.log(error.toString());
    this.emit('end');
}

gulp.task('default', ['gulp_watch']);

gulp.task('gulp_watch', function () {
    gulp.watch('./public/src/sass/**/*.scss', ['styles']);
    gulp.watch('./public/src/js/**/*.js', ['scripts']);
    gulp.watch('./admin/public/src/scss/**/*.scss', ['styles_admin']);
    gulp.watch('./admin/public/src/js/**/*.js', ['scripts_admin']);
});

gulp.task('styles', function () {
    return gulp.src('./public/src/sass/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .on('error', swallowError)
        .pipe(autoprefixer({
            browsers: ['last 20 versions', '> 5%'],
            cascade: false
        }))
        .pipe(cleanCSS())
        .pipe(rename('style.min.css'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./public/css'));
});

gulp.task('scripts', function() {
    return gulp.src('./public/src/js/app.js')
        .pipe(include())
        .pipe(sourcemaps.init())
        .pipe(rename('app.min.js'))
        .pipe(babel({
            presets: ['es2015']
        }))
        .on('error', swallowError)
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./public/js'));
});

gulp.task('styles_admin', function () {
    return gulp.src('./admin/public/src/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .on('error', swallowError)
        .pipe(cleanCSS())
        .pipe(rename('style.min.css'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./admin/public/css'));
});

gulp.task('scripts_admin', function() {
    return gulp.src('./admin/public/src/js/index.js')
        .pipe(include())
        .pipe(rename('app.min.js'))
        .on('error', swallowError)
        .pipe(uglify()) //минифицируем js файл
        .pipe(gulp.dest('./admin/public/js'));   //сохраняем минифицированную версию
});