const express = require('express'),
    router = express.Router(),
    modelBlog = require('../models/blog'),
    modelMenu = require('../models/menus'),
    helper = require('../ext/helper');

function getTopMenu(){
    return new Promise(function(resolve, reject){

        let result = modelMenu.getMenuItems(1);
        resolve(result);
        reject('error 1 in routes/blog.js');

    });
}

router.get('/', function(req, res, next) {

    let data;
    let rootPath = req.baseUrl.substr(1),
        numPage = parseInt(req.query.page);
    if(numPage < 2){
        return res.redirect(req.baseUrl);
    }

    function blogContent(){
        return new Promise(function(resolve, reject){

            let result = modelBlog.getListItems({
                rootPath,
                numPage,
                res
            });

            resolve(result);
            reject('error 2 in routes/blog.js');

        });
    }

    blogContent()
        .then(response => {
            data = response;
            return getTopMenu();
        })
        .then( response => {

            data.title = 'Category';
            data.menu = response;
            res.render('pages/blog', data);

        })
        .catch(fail => console.log(fail));
    
});

router.get('/:item', function(req, res, next) {

    let data;

    function getBlogItem() {
        return new Promise((resolve, reject) => {

            resolve(modelBlog.getItem(req.params.item));
            reject('error in routers/blog.js 1');
        });
    }

    getBlogItem()
        .then( response => {
            data = response;
            return getTopMenu();
        })
        .then( response => {
            data.menu = response;
            res.render('pages/item', data);
        })
        .catch(error => console.log(error));

});

module.exports = router;