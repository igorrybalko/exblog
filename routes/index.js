let express = require('express'),
    modelMenu = require('../models/menus'),
    router = express.Router();


/* GET home page. */
router.get('/', function(req, res, next) {


    let topMenu =  new Promise(function(resolve, reject){

      let result = modelMenu.getMenuItems(1);
      resolve(result);
      reject('error in routes/index.js');

    });


  topMenu
      .then(response => {
          return (

              res.render('pages/index', {
                  title: 'Homepage',
                  menu: response

              })
          );
      })
      .catch(fail => console.log(fail));
    
});

module.exports = router;
