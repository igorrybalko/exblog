const conf = require('../config'),
    mysql = require('mysql');
    helper = require('./helper');

let pool = mysql.createPool({
    host     : conf.get('db-host'),
    user     : conf.get('db-user'),
    password : conf.get('db-pass'),
    database : conf.get('db'),
    connectionLimit : 10
});

module.exports = pool;