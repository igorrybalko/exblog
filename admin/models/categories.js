const pool = require('../../ext/db'),
    helper = require('../../ext/helper');

function getListCategories(res, view) {

    pool.getConnection((err, connection) => {

        let query = 'SELECT * FROM `categories`';
        connection.connect();
        connection.query( query, (error, results, fields) => {
            connection.release();
            if (error) throw error;

            let data = {
                title: 'Categories',
                list: results
            };

            res.render(view, data);
        });
    });
}

function getListItems(res, view, id, rootPath, numPage) {
    pool.getConnection((err, connection) => {

        let query = 'SELECT COUNT(`id`) FROM `items` WHERE `category` = ' + id;
        connection.connect();
        connection.query( query, (error, countObj, fields) => {
            connection.release();
            if (error) throw error;

            _getListItems(res, view, id, rootPath, numPage, countObj[0]["COUNT(`id`)"]);
        });
    });
}

function _getListItems(res, view, id, rootPath, numPage, countItems){

    let itemsOnPage = 5;
    let countPages = Math.ceil(countItems/itemsOnPage);
    let curPage = 1;

    if(numPage > countPages){
        return res.redirect('/' + rootPath);
    }

    if(numPage > 0 && numPage <= countPages){
        curPage = numPage;
    }

    let start = (curPage - 1) * itemsOnPage;
    let pagination = {
        countPages,
        rootPath,
        curPage
    };

    pool.getConnection((err, connection) => {

        let query = 'SELECT * FROM `items` WHERE `category` = ' + id + ' LIMIT ' + start + ', ' + itemsOnPage;

        connection.connect();
        connection.query( query, (error, results, fields) => {
            connection.release();
            if (error) throw error;

            _getCategory(res, view, id, results, pagination);
        });
    });
}

function _getCategory(res, view, id, list, pagination){

    pool.getConnection((err, connection) => {

        let query = 'SELECT `description`, `title`, `alias` FROM `categories` WHERE `id` = ' + id;
        connection.connect();
        connection.query( query, (error, results, fields) => {
            connection.release();
            if (error) throw error;
            let data = results[0];
            data.currCatId = id;
            data.items = list;
            data.pagination = pagination;
            data.newCategory = '';
            res.render(view, data);
        });
    });
}

function getItem(res, view, itemId, catId){

    pool.getConnection((err, connection) => {

        let query = 'SELECT * FROM `items` WHERE `id` = ' + itemId + ' AND `category`  = ' + catId;

        connection.connect();
        connection.query( query, (error, results, fields) => {
            connection.release();
            if (error) throw error;
            let data = results[0];
            data.newItem = '';
            res.render(view, data);
        });
    });
}

function updateItem(res, data){
    pool.getConnection((err, connection) => {

        let query = 'UPDATE `items` SET `title` = "' + data.title
            + '", `content` = "' + helper.mysql_real_escape_string(data.content)
            + '", `alias` = "' + data.alias
            + '", `fullimg` = "' + data.fullimg
            + '", `introimg` = "' + data.introimg
            + '" WHERE `id` = ' + data.id + ' AND `category` = ' + data.catId;

        connection.connect();
        connection.query( query, (error, results, fields) => {
            connection.release();
            if (error) throw error;

            res.redirect('/admin/categories/' + data.catId + '/' + data.id);
        });
    });
}

function updateCategory(res, data){
    pool.getConnection((err, connection) => {

        let query = 'UPDATE `categories` SET `title` = "' + data.title
            + '", `description` = "' + data.description + '", `alias` = "' + data.alias + '" WHERE `id` = ' + data.id;

        connection.connect();
        connection.query( query, (error, results, fields) => {
            connection.release();
            if (error) throw error;

            res.redirect('/admin/categories/' + data.id);
        });
    });
}

function removeItem(res, data){
    pool.getConnection((err, connection) => {
        
        let query = 'DELETE FROM `items` WHERE `id` = ' + data.id + ' AND `category` = ' + data.catId;

        connection.connect();
        connection.query( query, (error, results, fields) => {
            connection.release();
            if (error) throw error;

            res.redirect('/admin/categories/' + data.catId);
        });
    });
}

function createItem(res, data) {
    pool.getConnection((err, connection) => {

        let query = "INSERT INTO `items` (`title`, `category`, `content`, `fullimg`, `introimg`, `alias`) " +
            "VALUES ('"
            + data.title + "', '"
            + data.catId + "', '"
            + data.content + "', '"
            + data.fullimg + "', '"
            + data.introimg + "', '"
            + data.alias
            + "')";

        connection.connect();
        connection.query( query, (error, results, fields) => {
            connection.release();
            if (error) throw error;

            res.redirect('/admin/categories/' + data.catId);
        });
    });
}

function createCategory(res, data) {
    pool.getConnection((err, connection) => {

        let query = "INSERT INTO `categories` (`title`, `alias` ,`description`) " +
            "VALUES ('" + data.title + "', '" + data.alias + "', '" + data.description + "')";

        console.log(query);

        connection.connect();
        connection.query( query, (error, results, fields) => {
            connection.release();
            if (error) throw error;

            res.redirect('/admin/categories');
        });
    });
}

exports.getItem = getItem;
exports.getListItems = getListItems;
exports.getListCategories = getListCategories;
exports.updateItem = updateItem;
exports.removeItem = removeItem;
exports.updateCategory = updateCategory;
exports.createItem = createItem;
exports.createCategory = createCategory;