const pool = require('../../ext/db'),
    helper = require('../../ext/helper');

function getListMenus(res, view) {

    pool.getConnection((err, connection) => {

        let query = 'SELECT * FROM `menus`';
        connection.connect();
        connection.query( query, (error, results, fields) => {
            connection.release();
            if (error) throw error;

            let data = {
                title: 'Menus',
                list: results
            };

            res.render(view, data);
        });
    });
}

function getListItems(res, view, id, rootPath, numPage) {
    pool.getConnection((err, connection) => {

        let query = 'SELECT COUNT(`id`) FROM `menuitems` WHERE `menuid` = ' + id;

        connection.connect();
        connection.query( query, (error, countObj, fields) => {
            connection.release();
            if (error) throw error;

            _getListItems(res, view, id, rootPath, numPage, countObj[0]["COUNT(`id`)"]);
        });
    });
}

function _getListItems(res, view, id, rootPath, numPage, countItems){

    let itemsOnPage = 5;
    let countPages = Math.ceil(countItems/itemsOnPage);
    let curPage = 1;

    if(numPage > countPages){
        return res.redirect('/' + rootPath);
    }

    if(numPage > 0 && numPage <= countPages){
        curPage = numPage;
    }

    let start = (curPage - 1) * itemsOnPage;
    let pagination = {
        countPages,
        rootPath,
        curPage
    };

    pool.getConnection((err, connection) => {

        let query = 'SELECT * FROM `menuitems` WHERE `menuid` = ' + id + ' LIMIT ' + start + ', ' + itemsOnPage;

        connection.connect();
        connection.query( query, (error, results, fields) => {
            connection.release();
            if (error) throw error;

            _getMenu(res, view, id, results, pagination);
        });
    });
}
function _getMenu(res, view, id, list, pagination){

    pool.getConnection((err, connection) => {

        let query = 'SELECT `title` FROM `menus` WHERE `id` = ' + id;
        connection.connect();
        connection.query( query, (error, results, fields) => {
            connection.release();
            if (error) throw error;
            let data = results[0];
            data.menuId = id;
            data.items = list;
            data.pagination = pagination;
            res.render(view, data);
        });
    });
}

function getItem(res, view, itemId, menuId){

    pool.getConnection((err, connection) => {

        let query = 'SELECT * FROM `menuitems` WHERE `id` = ' + itemId + ' AND `menuid`  = ' + menuId;

        connection.connect();
        connection.query( query, (error, results, fields) => {
            connection.release();
            if (error) throw error;
            let data = results[0];
            data.newItem = '';
            res.render(view, data);
        });
    });
}

function updateItem(res, data){
    pool.getConnection((err, connection) => {

        let query = 'UPDATE `menuitems` SET `title` = "' + data.title + '", `published` = "' + data.published
            + '", `url` = "' + data.url + '" WHERE `id` = ' + data.id + ' AND `menuid` = ' + data.menuId;

        connection.connect();
        connection.query( query, (error, results, fields) => {
            connection.release();
            if (error) throw error;

            res.redirect('/admin/menus/' + data.menuId + '/' + data.id);
        });
    });
}

function removeItem(res, data){
    pool.getConnection((err, connection) => {

        let query = 'DELETE FROM `menuitems` WHERE `id` = ' + data.id + ' AND `menuid` = ' + data.menuId;

        connection.connect();
        connection.query( query, (error, results, fields) => {
            connection.release();
            if (error) throw error;

            res.redirect('/admin/menus/' + data.menuId);
        });
    });
}

function createItem(res, data) {
    pool.getConnection((err, connection) => {

        let query = "INSERT INTO `menuitems` (`title`, `url`, `menuid`, `published`) " +
            "VALUES ('" + data.title + "', '" + data.url + "', '" + data.menuId + "', '" + data.published + "')";
        console.log(query);
        connection.connect();
        connection.query( query, (error, results, fields) => {
            connection.release();
            if (error) throw error;

            res.redirect('/admin/menus/' + data.menuId);
        });
    });
}

function updateMenu(res, data){
    pool.getConnection((err, connection) => {

        let query = 'UPDATE `menus` SET `title` = "' + data.title
            + '", WHERE `id` = ' + data.id;

        connection.connect();
        connection.query( query, (error, results, fields) => {
            connection.release();
            if (error) throw error;

            res.redirect('/admin/menus/' + data.id);
        });
    });
}

exports.getItem = getItem;
exports.getListItems = getListItems;
exports.getListMenus = getListMenus;
exports.updateItem = updateItem;
exports.removeItem = removeItem;
exports.createItem = createItem;
exports.updateMenu = updateMenu;