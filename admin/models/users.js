const crypt = require('crypto'),
    pool = require('../../ext/db');

function _getHash(password, salt, iteration){
    let c = crypt.createHmac('sha1', salt);
    for(let i = 0; i < iteration; i++){
        c = c.update(password);
    }
    return c.digest('hex');
}

function _checkPassword(password, salt, iteration, hash){
    return _getHash(password, salt, iteration) === hash;
}

function checkPass(req, res, login, pass){

    pool.getConnection((err, connection) => {

        let query = 'SELECT `id`, `hashpass`, `salt`, `iteration` FROM `users` WHERE `login` = "' + login + '"';
        connection.connect();
        connection.query( query, (error, result, fields) => {
            connection.release();

            if (error) throw error;

            if(_checkPassword(pass, result[0].salt, result[0].iteration, result[0].hashpass)){
                req.session.user = result[0].id;
                res.render('pages/index', { title: 'Dashboard'});
            }else{
                res.render('pages/login', { title: 'Express', error: 'Wrong password' });
            }
        });
    });
}

exports.checkPass = checkPass;