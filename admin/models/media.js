const fs = require('fs'),
    helper = require('../../ext/helper'),
    rootFolder = __dirname + '/../../public/images/content';


function getFileManager(message = '', path = rootFolder, currFolder = '/') {

    let data = {};
    let filesList = fs.readdirSync(path);
    let folders = [];
    let files = [];
    filesList.forEach(file => {

        if(file.indexOf('.') == -1){
            folders.push({name: file,
                type: 'folder',
                path: path + '/' + file
            });
        }else{
            files.push({name: file, type: 'file'});
        }
    });

    let containe = folders.concat(files);

    data.containe = containe;
    data.title = 'media';
    data.currFolder = currFolder;
    data.message = message;
    data.path = rootFolder;

    return data;
    
}

function createFolder(nameFolder, currFolder) {

    if(nameFolder.trim()){
        let fullPathCurrFolder = rootFolder + currFolder;
        fs.mkdirSync(fullPathCurrFolder + '/' + nameFolder);
        let message = {
            type: 'success',
            text: 'folder created'
        };
        return getFileManager(message, fullPathCurrFolder, currFolder)
    }else {
        return 'error create folder';
    }
}

exports.getFileManager = getFileManager;
exports.createFolder = createFolder;