const pool = require('../../ext/db'),
    helper = require('../../ext/helper');

function getArticles(res, view, rootPath, numPage) {
    pool.getConnection((err, connection) => {

        let query = 'SELECT COUNT(`id`) FROM `simplepages`';
        connection.connect();
        connection.query( query, (error, countObj, fields) => {
            connection.release();
            if (error) throw error;

            _getArticles(res, view, rootPath, numPage, countObj[0]["COUNT(`id`)"]);
        });
    });
}

function _getArticles(res, view, rootPath, numPage, countItems){

    let itemsOnPage = 5;
    let countPages = Math.ceil(countItems/itemsOnPage);
    let curPage = 1;

    if(numPage > countPages){
        return res.redirect('/' + rootPath);
    }

    if(numPage > 0 && numPage <= countPages){
        curPage = numPage;
    }

    let start = (curPage - 1) * itemsOnPage;
    let pagination = {
        countPages,
        rootPath,
        curPage
    };

    pool.getConnection((err, connection) => {

        let data = {};
        let query = 'SELECT * FROM `simplepages` LIMIT ' + start + ', ' + itemsOnPage;
        connection.connect();
        connection.query( query, (error, results, fields) => {
            connection.release();
            if (error) throw error;

            data.items = results;
            data.pagination = pagination;
            data.title = "Articles";
            res.render(view, data);
        });
    });
}

function getArticle(res, view, itemId){

    pool.getConnection((err, connection) => {

        let query = 'SELECT * FROM `simplepages` WHERE `id` = ' + itemId;

        connection.connect();
        connection.query( query, (error, results, fields) => {
            connection.release();
            if (error) throw error;
            let data = results[0];
            data.newItem = '';
            res.render(view, data);
        });
    });
}

function updateArticle(res, data){
    pool.getConnection((err, connection) => {

        let query = 'UPDATE `simplepages` SET `title` = "' + data.title
            + '", `content` = "' + data.content
            + '", `alias` = "' + data.alias
            + '" WHERE `id` = ' + data.id;

        connection.connect();
        connection.query( query, (error, results, fields) => {
            connection.release();
            if (error) throw error;

            res.redirect('/admin/articles/' + data.id);
        });
    });
}

function removeArticle(res, data){
    pool.getConnection((err, connection) => {

        let query = 'DELETE FROM `simplepages` WHERE `id` = ' + data.id;

        connection.connect();
        connection.query( query, (error, results, fields) => {
            connection.release();
            if (error) throw error;

            res.redirect('/admin/articles/');
        });
    });
}

function createArticle(res, data) {
    pool.getConnection((err, connection) => {

        let query = "INSERT INTO `simplepages` (`title`, `content`) " +
            "VALUES ('"
            + data.title + "', '"
            + data.alias + "', '"
            + data.content + "')";

        connection.connect();
        connection.query( query, (error, results, fields) => {
            connection.release();
            if (error) throw error;

            res.redirect('/admin/articles/');
        });
    });
}

exports.getArticle = getArticle;
exports.getArticles = getArticles;
exports.updateArticle = updateArticle;
exports.removeArticle = removeArticle;
exports.createArticle = createArticle;