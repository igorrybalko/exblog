function transInMediaManager(path, currFolder) {
    $.post(location.href, {
        path,
        currFolder,
        task: 'trans'
    }).done( (response) => {

        $('.media-manag-wr').html(response);

    }).fail( (data) => {
        alert('error');
        console.log('error: ' + data);
    });
}
$(document).ready(function () {



    $('#page-close').on('click', function () {
        location.assign($(this).data('href'));
    });

    $('#item-save').on('click', function () {
        $('[name="task"]').val('save');
        $('#item-form').submit();
    });

    $('#item-remove').on('click', function () {
        $('[name="task"]').val('remove');
        $('#item-form').submit();
    });

    $('#item-create').on('click', function () {
        $('[name="task"]').val('create');
        $('#item-form').submit();
    });

    $('#cat-save').on('click', function () {
        $('[name="task"]').val('save');
        $('#category-form').submit();
    });

    $('#cat-create').on('click', function () {
        $('[name="task"]').val('create');
        $('#category-form').submit();
    });

    $('#menu-save').on('click', function () {
        $('[name="task"]').val('save');
        $('#menu-form').submit();
    });

    tinymce.init({
        selector: '.textarea-editor',  // change this value according to your HTML
        plugins: 'codesample',
        toolbar: 'codesample'
    });

    $('.media-manag-wr').on('click', '.m-folder', function(){

        let prePath = $('.curr-folder-media').text(),
            path = $(this).data('path');

        if(prePath.length < 2){
            prePath = ''
        }

        let currFolder = prePath + '/' + $(this).data('name');

        transInMediaManager(path, currFolder);

    });

    $('.media-manag-wr').on('click', '.m-create-folder', function(){

        let newFolder = $('#newfolder');

        if(!newFolder.val().trim()){

            newFolder.parent().addClass('has-error');

        }else{

            let prePath = $('.curr-folder-media').text();
            
            $.post(location.href, {
                currFolder: prePath,
                task: 'createFolder',
                nameFolder: newFolder.val()
            }).done( (response) => {

                $('.media-manag-wr').html(response);

            }).fail( (data) => {
                alert('error');
                console.log(data);
            });
            
        }

    });

    $('.media-manag-wr').on('click', '.backinfm', function(){

        let path = $('.curr-folder-media').text(),
            prevFolder = '',
            currFolder = '/';
        let pathArr = path.split('/');
        pathArr.shift();

        if(pathArr.length > 1){
            prevFolder = '/' + pathArr[pathArr.length - 2];
            if(pathArr.length == 2){
                currFolder = '/' + pathArr[0];
            }else{
                currFolder = '/' + pathArr.join('/');
            }
        }

        let fullPath = $(this).data('path') + prevFolder;

        transInMediaManager(fullPath, currFolder);

    });

    $('.media-manag-wr').on('click', '.delete-file', function(){
        
            let prePath = $('.curr-folder-media').text();

            $.post(location.href, {
                currFolder: prePath,
                task: 'deleteFile',
                nameFolder: newFolder.val()
            }).done( (response) => {

                $('.media-manag-wr').html(response);

            }).fail( (data) => {
                alert('error');
                console.log(data);
            });

    });
});