let express = require('express'),
    router = express.Router(),
    mediaModel = require('../models/media'),
    helper = require('../../ext/helper');


router.get('/', function(req, res, next) {
    if(req.session.user){
        res.render('pages/media', mediaModel.getFileManager());
    }else{
        res.redirect('/admin');
        res.end();
    }
});

router.post('/', function(req, res, next) {

    if(req.session.user){

        switch (req.body.task){
            case 'createFolder':
                res.render('includes/mediapart', mediaModel.createFolder(req.body.nameFolder, req.body.currFolder));
                break;
            case 'upload':

                if (!req.files)
                    return res.status(400).send('No files were uploaded.');

                let newfile = req.files.newfile;

                newfile.mv( __dirname + '/../../public/images/content' + req.body.currfolder + '/' + newfile.name, function(err) {
                    if (err)
                        return res.status(500).send(err);

                    let message = {
                        type: 'success',
                        text: 'file uploded'
                    };
                    res.render('pages/media', mediaModel.getFileManager(message));
                });
                
                break;
            case 'trans':
                res.render('includes/mediapart', mediaModel.getFileManager('', req.body.path, req.body.currFolder));
                break;
            default:
                res.render('includes/mediapart', mediaModel.getFileManager());
        }

    }else{
        res.redirect('/admin');
        res.end();
    }
});

module.exports = router;