var express = require('express'),
    menusModel = require('../models/menus.js');
var router = express.Router();

router.get('/', function(req, res, next) {
    if(req.session.user){
        menusModel.getListMenus(res, 'pages/menus');
    }else{
        res.redirect('/admin');
        res.end();
    }

});

router.get('/:id', function(req, res, next) {
    if(req.session.user){

        let rootPath = req.baseUrl.substr(1) + '/' + req.params.id,
            numPage = parseInt(req.query.page);
        if(numPage < 2){
            return res.redirect('/' + rootPath);
        }
        menusModel.getListItems(res, 'pages/menu', req.params.id, rootPath, numPage);
    }else{
        res.redirect('/admin');
        res.end();
    }

});

router.get('/:id/:itemId', function(req, res, next) {
    if(req.session.user){

        if(req.params.itemId == 'new-item'){

            let data = {
                menuid: req.params.id,
                url: '',
                title: '',
                id: '',
                newItem: 1,
                published: ''
            };

            res.render('pages/menuitem', data);

        }else{
            menusModel.getItem(res, 'pages/menuitem', req.params.itemId, req.params.id);
        }

    }else{
        res.redirect('/admin');
        res.end();
    }

});

router.post('/:id/:itemId', function(req, res, next) {
    if(req.session.user && req.body.type == 'menuitem'){

        switch (req.body.task){
            case 'save':
                if(req.params.itemId == req.body.id && req.params.id == req.body.menuid){

                    let data = {
                        id: req.body.id,
                        menuId:  req.body.menuid,
                        url: req.body.url,
                        title: req.body.title,
                        published: req.body.published
                    };

                    menusModel.updateItem(res, data);
                }
                break;
            case 'remove':
                if(req.params.itemId == req.body.id && req.params.id == req.body.menuid){

                    let data = {
                        id: req.body.id,
                        menuId:  req.body.menuid
                    };

                    menusModel.removeItem(res, data);
                }
                break;
            case 'create':
                if(req.params.id == req.body.menuid){

                    let data = {
                        menuId:  req.body.menuid,
                        url: req.body.url,
                        title: req.body.title,
                        published: req.body.published
                    };
                    menusModel.createItem(res, data);
                }
                break;
        }

    }else{
        res.redirect('/admin');
        res.end();
    }

});

router.post('/:id', function(req, res, next) {
    if(req.session.user){

        if(req.body.task == 'save' && req.body.type == 'menu') {

            if(req.params.id == req.body.id){

                let data = {
                    id: req.body.id,
                    title: req.body.title
                };

                menusModel.updateMenu(res, data);
            }
        }

    }else{
        res.redirect('/admin');
        res.end();
    }

});

module.exports = router;