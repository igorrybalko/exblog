var express = require('express'),
    categoriesModel = require('../models/categories.js');
var router = express.Router();

router.get('/', function(req, res, next) {
    if(req.session.user){
        categoriesModel.getListCategories(res, 'pages/categories');
    }else{
        res.redirect('/admin');
        res.end();
    }

});

router.get('/:id', function(req, res, next) {
    if(req.session.user){

        if(req.params.id == 'new-category'){

            let data = {
                newCategory: 1,
                title: '',
                description: '',
                alias: ''
            };

            res.render('pages/category', data);

        }else{
            let rootPath = req.baseUrl.substr(1) + '/' + req.params.id,
                numPage = parseInt(req.query.page);
            if(numPage < 2){
                return res.redirect('/' + rootPath);
            }
            categoriesModel.getListItems(res, 'pages/category', req.params.id, rootPath, numPage);
        }


    }else{
        res.redirect('/admin');
        res.end();
    }

});

router.post('/:id', function(req, res, next) {
    if(req.session.user && req.body.type == 'category'){

        if(req.body.task == 'save') {

            if(req.params.id == req.body.id){

                let data = {
                    id: req.body.id,
                    description: req.body.description,
                    title: req.body.title,
                    alias: req.body.alias
                };

                categoriesModel.updateCategory(res, data);
            }
        }
        if(req.body.task == 'create'){
            let data = {
                description: req.body.description,
                title: req.body.title,
                alias: req.body.alias
            };
            categoriesModel.createCategory(res, data);
        }

    }else{
        res.redirect('/admin');
        res.end();
    }

});

router.get('/:id/:itemId', function(req, res, next) {
    if(req.session.user){

        if(req.params.itemId == 'new-item'){
            
            let data = {
                category: req.params.id,
                content: '',
                title: '',
                id: '',
                newItem: 1,
                alias: '',
                fullimg: '',
                introimg: ''
            };

            res.render('pages/item', data);

        }else{
            categoriesModel.getItem(res, 'pages/item', req.params.itemId, req.params.id);
        }

    }else{
        res.redirect('/admin');
        res.end();
    }

});

router.post('/:id/:itemId', function(req, res, next) {
    if(req.session.user && req.body.type == 'item'){

        switch (req.body.task){
            case 'save':
                if(req.params.itemId == req.body.id && req.params.id == req.body.catid){

                    let data = {
                        id: req.body.id,
                        catId:  req.body.catid,
                        content: req.body.content,
                        title: req.body.title,
                        alias: req.body.alias,
                        fullimg: req.body.fullimg,
                        introimg: req.body.introimg
                    };

                    categoriesModel.updateItem(res, data);
                }
                break;
            case 'remove':
                if(req.params.itemId == req.body.id && req.params.id == req.body.catid){

                    let data = {
                        id: req.body.id,
                        catId:  req.body.catid
                    };

                    categoriesModel.removeItem(res, data);
                }
                break;
            case 'create':
                if(req.params.id == req.body.catid){

                    let data = {
                        catId:  req.body.catid,
                        content: req.body.content,
                        title: req.body.title,
                        alias: req.body.alias,
                        fullimg: req.body.fullimg,
                        introimg: req.body.introimg
                    };
                    categoriesModel.createItem(res, data);
                }
                break;
        }

    }else{
        res.redirect('/admin');
        res.end();
    }

});

module.exports = router;