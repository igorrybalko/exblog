var express = require('express');
var router = express.Router();
var modelAuth = require('../models/users');

router.get('/', function(req, res, next) {
  if(req.session.user){
    res.render('pages/index', { title: 'Dashboard'});
  }else{
    res.render('pages/login', { title: 'Express', error: null });
  }

});

router.post('/', function(req, res, next){
  var login = req.body.login,
    pass = req.body.password;
  if(login && pass){
    modelAuth.checkPass(req, res, login, pass);
  }else{
    res.render('pages/login', { title: 'Express', error: 'Error' });
  }
  
});

router.get('/logout', function(req, res, next){

  req.session.destroy(function(err) {
    if(err) next(err);
  });

  res.redirect('/');
  res.end();

});

module.exports = router;