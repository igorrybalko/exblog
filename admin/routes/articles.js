var express = require('express'),
    articlesModel = require('../models/articles');
var router = express.Router();

router.get('/', function(req, res, next) {


    if(req.session.user){

        let rootPath = req.baseUrl.substr(1),
            numPage = parseInt(req.query.page);
        if(numPage < 2){
            return res.redirect('/' + rootPath);
        }
        articlesModel.getArticles(res, 'pages/articles', rootPath, numPage);
    }else{
        res.redirect('/admin');
        res.end();
    }

});

router.get('/:id', function(req, res, next) {
    if(req.session.user){

        if(req.params.id == 'new-item'){

            let data = {
                content: '',
                title: '',
                id: '',
                newItem: 1,
                alias: ''
            };

            res.render('pages/article', data);

        }else{
            articlesModel.getArticle(res, 'pages/article', req.params.id);
        }

    }else{
        res.redirect('/admin');
        res.end();
    }
});

router.post('/:id', function(req, res, next) {
    if(req.session.user && req.body.type == 'article'){

        switch (req.body.task){
            case 'save':
                if(req.params.id == req.body.id){

                    let data = {
                        id: req.body.id,
                        content: req.body.content,
                        title: req.body.title
                    };

                    articlesModel.updateArticle(res, data);
                }
                break;
            case 'remove':
                if(req.params.id == req.body.id){

                    let data = {
                        id: req.body.id
                    };

                    articlesModel.removeArticle(res, data);
                }
                break;
            case 'create':
                if(req.params.id == 'new-item'){

                    let data = {
                        catId:  req.body.catid,
                        content: req.body.content,
                        title: req.body.title,
                        alias: req.body.alias
                    };
                    articlesModel.createArticle(res, data);
                }
                break;
        }

    }else{
        res.redirect('/admin');
        res.end();
    }

});

module.exports = router;