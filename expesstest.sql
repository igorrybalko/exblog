-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Апр 27 2018 г., 21:30
-- Версия сервера: 10.1.10-MariaDB
-- Версия PHP: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `expesstest`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `description` text,
  `createdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `title`, `alias`, `description`, `createdate`) VALUES
(1, 'Блог', 'blog', '<p>Описание блога <strong>Описание блога</strong> Описание блога Описание блога Описание блога Описание блога Описание блога Описание блога Описание блога Описание блога Описание блога Описание блога Описание блога Описание блога Описание блога</p>\r\n<p>Описание блога Описание блога Описание блога Описание блога Описание блога Описание блога Описание блога Описание блога Описание блога Описание блога Описание блога Описание блога Описание блога</p>', '2017-04-24 21:00:06'),
(2, 'Первые блюда', 'pervye-bliuda', '<p>some description fg</p>', '2017-07-16 11:41:39'),
(3, 'Салаты', 'salaty', '', '2017-09-02 20:46:34'),
(4, 'Напитки', 'napitki', '', '2017-09-03 09:03:28');

-- --------------------------------------------------------

--
-- Структура таблицы `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `category` int(11) NOT NULL,
  `content` text NOT NULL,
  `introimg` varchar(255) NOT NULL,
  `fullimg` varchar(255) NOT NULL,
  `createdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `items`
--

INSERT INTO `items` (`id`, `title`, `alias`, `category`, `content`, `introimg`, `fullimg`, `createdate`) VALUES
(1, 'Новость 1 rr dfghfhfh', 'news1', 1, '<p>текст новости tyryy fhfhhfvffffffffffffffffffffffffffffff fdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddvf</p>', '', '', '2017-04-26 14:30:00'),
(5, 'a Новость 1 c', 'news55', 1, 'текст новости', '', '', '2017-04-26 14:30:00'),
(6, 'news 2', 'news34', 1, 'текст новости', '', '', '2017-05-08 14:30:00'),
(8, 'news 4', 'news42', 1, 'текст новости', '', '', '2017-05-08 17:12:07'),
(9, 'qwerrtt', 'sadfsf', 1, 'dfvdfvdfvdfvdfv', '', '', '2017-05-13 20:29:43'),
(10, 'qrtrt', 'ytytadfsf', 1, 'dfvdfvdfvdfvdfv rtgr', '', '', '2017-05-13 20:30:05'),
(11, 'caption1', 'vbhjtyt', 1, 'vcgheewweffffffffff gt ggggggg', '', '', '2017-05-13 20:33:06'),
(12, 'caption 2', 'zpoik', 1, 'gfgy trr  rtr', '', '', '2017-05-13 20:33:06'),
(13, 'hghghghgh', 'ghjtyt', 1, 'vcgheewweffffffffff gt ggggggg', '', '', '2017-05-13 20:33:15'),
(14, 'poiuy', 'poik', 1, 'gfgy trr  rtr', '', '', '2017-05-13 20:33:15'),
(15, 'из второй категории тест fcfd', 'kfpe', 2, 'we wer werkgk ktkg ktkgkrfgr ', '', '', '2017-05-13 21:49:58'),
(16, 'Новая статья самая', '', 1, 'Какой-то текст', '', '', '2017-07-30 13:16:49'),
(18, 'еще новая статья апрарар', '', 1, 'укепмм пипипи ап апр', '', '', '2017-07-30 13:22:52'),
(19, 'Оливье', 'olivie', 3, '<p>Салат "Оливье" давно стал обязательным блюдом на новогоднем столе. Салат был придуман в шестидесятые годы XIX века кулинаром из Франции Люсьеном Оливье. На тот момент Люсьен Оливье владел рестораном "Эрмитаж" в Москве, который находился на Трубной площади.</p>\r\n<p>В состав оригинального салата водили мясо рыбчика, картофель, огурец, оливки, провансаль, каперсы, мясо раков, вареннные яйца. Бытует легенда, что изначально салат "Оливье" подавался в виде украшенной кулинарной композиции. Но посететели ресторана оценили лишь вкусовые достоинства салата, а не его внешний вид. И Люсьен Оливье решил подавать салат в превычном для нас перемешаном виде. С приходом советской власти рецепт притерпел изменения. Но сам салат еще более влился в наш рацион. Состав классического советского "Оливье" следующий: картофель, морковь, огурец, колбаса вареная, консервированный зеленый горошек, майонез, соль. Огурец обычно берется консервированный, но также можно наблюдать в салате и свежий. Колбасу заменяют говядиной или курятиной. Так же иногда добавляют маринованные шампиньены, зелень и другие специи. В следствии этого салат "Оливье" остается не очень дорогим блюдом для празничного и обычного стола. В Европе "Оливье" называют "Русский салат"</p>', '/images/content/salads/olivie.jpg', '/images/content/salads/olivie.jpg', '2017-09-02 20:48:13');

-- --------------------------------------------------------

--
-- Структура таблицы `menuitems`
--

CREATE TABLE `menuitems` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `menuid` int(11) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `createdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `menuitems`
--

INSERT INTO `menuitems` (`id`, `title`, `url`, `menuid`, `published`, `createdate`) VALUES
(1, 'Салаты', '/salaty', 1, 1, '2017-08-09 21:33:44'),
(2, 'Напитки', '/napitki', 1, 1, '2017-08-09 21:33:44'),
(3, 'Первые блюда', '/pervye-bliuda', 1, 1, '2017-08-09 21:33:52'),
(4, 'tot1', '/ddffg1', 1, 1, '2017-08-09 21:33:52'),
(5, 'drereer', 'erererererere', 1, 0, '2017-08-12 12:49:00'),
(6, 'eretetert', 'hgfngngnh', 1, 1, '2017-08-12 12:56:02');

-- --------------------------------------------------------

--
-- Структура таблицы `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `createdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `published` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `menus`
--

INSERT INTO `menus` (`id`, `title`, `createdate`, `published`) VALUES
(1, 'top', '2017-08-07 22:06:06', 1),
(2, 'test', '2017-08-07 22:06:27', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `simplepages`
--

CREATE TABLE `simplepages` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `createdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `hashpass` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `iteration` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `role` int(11) NOT NULL,
  `createdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `hashpass`, `salt`, `iteration`, `name`, `role`, `createdate`) VALUES
(2, 'adminBlog', '1aff4d10b9aa1f652cbd0b0525a9a724e1033d55', 'somesolt', 4, 'igor', 3, '2017-05-27 17:15:26');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menuitems`
--
ALTER TABLE `menuitems`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `simplepages`
--
ALTER TABLE `simplepages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT для таблицы `menuitems`
--
ALTER TABLE `menuitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `simplepages`
--
ALTER TABLE `simplepages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
