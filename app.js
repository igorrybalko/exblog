const express = require('express'),
  path = require('path'),
  favicon = require('serve-favicon'),
  logger = require('morgan'),
  cookieParser = require('cookie-parser'),
  session = require('express-session'),
  bodyParser = require('body-parser'),
  conf = require('./config');

var index = require('./routes/index'),
    users = require('./routes/users'),
    blog = require('./routes/blog'),
    admin = require('./admin/app');

var app = express();

// view engine setup
app.engine(conf.get('app-engine'), require('ejs-mate'));
app.set('views', path.join(__dirname, conf.get('app-view')));
app.set('view engine', conf.get('app-engine'));

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
  secret: conf.get('session:secret'),
  key: conf.get('session:key'),
  cookie: conf.get('session:cookie'),
  resave: true,
  saveUninitialized: true
}));

app.use('/', index);
app.use('/users', users);
app.use('/napitki', blog);
app.use('/salaty', blog);
app.use('/pervye-bliuda', blog);
app.use('/admin', admin);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('pages/error');
});

module.exports = app;
